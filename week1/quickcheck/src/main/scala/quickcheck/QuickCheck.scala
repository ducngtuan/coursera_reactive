package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {
  lazy val genHeap: Gen[H] = for {
    x <- arbitrary[Int]
    h <- oneOf(value(empty), genHeap)
  } yield insert(x, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  // Insert an element to a heap then minimum of heap should be this element
  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  // Insert 2 elements to empty heap then minimum of heap should be
  // minimum of the 2 elements
  property("min2") = forAll { (x: Int, y: Int) =>
    val h = insert(x, insert(y, empty))
    val m = findMin(h)
    if (x < y) m == x
    else m == y
  }

  // Minimum of a melding of any two should be minimum of one or the other
  property("min3") = forAll { (h1: H, h2: H) =>
    val m = findMin(meld(h1, h2))
    m == findMin(h1) || m == findMin(h2)
  }

  // Insert the minimum of a heap to itself then the minimum should be the same
  // (unless for empty heap)
  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  // Insert an element to an empty heap then remove should return empty heap
  property("insertThenDelete") = forAll { (x: Int) =>
    val h = deleteMin(insert(x, empty))
    isEmpty(h)
  }

  // Continually finding and delete minima from a heap should
  // return a sorted sequence
  property("continuallyFindMinShouldBeSorted") = forAll { (h: H) =>
    isSorted(h)
  }

  // Melding 2 empty heaps
  property("meld2EmptyHeap") = forAll { (h: H) =>
    isEmpty(meld(empty, empty))
  }

  def isSorted(h: H) = {
    def _isSorted(x: Int, h: H): Boolean =
      if (isEmpty(h)) true
      else {
        val m = findMin(h)
        x <= m && _isSorted(m, deleteMin(h))
      }

    if (isEmpty(h)) true
    else _isSorted(findMin(h), h)
  }

  def compareHeap(h1: H, h2: H): Boolean =
    if (isEmpty(h1) && isEmpty(h2)) true
    else
      if ((!isEmpty(h1) && isEmpty(h2)) || (isEmpty(h1) && !isEmpty(h2))) false
      else
        if (findMin(h1) == findMin(h2)) true
        else compareHeap(deleteMin(h1), deleteMin(h2))

  def toList(h: H): List[Int] =
    if (isEmpty(h)) List()
    else findMin(h) :: toList(deleteMin(h))

  // Melding 2 heaps
  property("meld1") = forAll { (h1: H, h2: H) =>
    isSorted(meld(h1, h2))
  }

  // Melding 2 heaps should preserve elements
  property("meld2") = forAll { (h1: H, h2: H) =>
    val h = meld(h1, h2)
    val l = toList(h)
    val l1 = toList(h1)
    val l2 = toList(h2)
    l.sorted == (l1 ++ l2).sorted
  }

  // Convert a list in to a heap, actually test insert, deleteMin, findMin
  property("insert") = forAll { (l: List[Int]) =>
    val h = l.foldLeft(empty)((h0, x) => insert(x, h0))
    toList(h).sorted == l.sorted
  }
}
