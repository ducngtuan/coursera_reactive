package simulations

import math.random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8

    // to complete: additional parameters of simulation
    val delayTime = 5

    val incubationTime = 6
    val dieTime = 14
    val immuneTime = 16
    val healTime = 18

    val prevalenceRate = 0.01
    val transRate = 0.4
    val dieRate = 0.25
  }

  import SimConfig._

  val persons: List[Person] = (1 to SimConfig.population).toList.map(new Person(_))
  persons.take((population * prevalenceRate).toInt).foreach(_.getInfected)

  class Person (val id: Int) {
    var infected = false
    var sick = false
    var immune = false
    var dead = false

    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)

    def move() {
      if (dead) return

      val directions = List(
        ((row + 1 + roomRows) % roomRows, col),
        ((row - 1 + roomRows) % roomRows, col),
        (row, (col + 1 + roomColumns) % roomColumns),
        (row, (col - 1 + roomColumns) % roomColumns))
        .filter { case (r, c) =>
          !persons.filter(p => p.row == r && p.col == c).exists(p => p.sick || p.dead)
        }
      if (!directions.isEmpty) {
        val choice = directions(randomBelow(directions.size))
        row = choice._1
        col = choice._2

        val roomMates = persons.filter(p => p.row == row && p.col == col)
        if (roomMates.exists(p => p.infected || p.sick)
            && !infected && !immune
            && random < transRate)
          getInfected()  
      }

      afterDelay(randomBelow(delayTime) + 1) { move }
    }
    afterDelay(randomBelow(delayTime) + 1) { move }

    def getInfected() {
      infected = true
      afterDelay(incubationTime) { sick = true }
      afterDelay(dieTime) { if (random < dieRate) { dead = true; infected = false } }
      afterDelay(immuneTime) { if (!dead) { immune = true; sick = false } }
      afterDelay(healTime) { if (!dead) { immune = false; infected = false; sick = false } }
    }
  }
}
