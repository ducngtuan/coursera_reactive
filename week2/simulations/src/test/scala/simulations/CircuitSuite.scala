package simulations

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CircuitSuite extends CircuitSimulator with FunSuite {
  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5
  
  test("andGate example") {
    val in1, in2, out = new Wire
    andGate(in1, in2, out)
    in1.setSignal(false)
    in2.setSignal(false)
    run
    
    assert(out.getSignal === false, "and 1")

    in1.setSignal(true)
    run
    
    assert(out.getSignal === false, "and 2")

    in2.setSignal(true)
    run
    
    assert(out.getSignal === true, "and 3")
  }

  test("orGate example") {
      val in1, in2, out = new Wire
      orGate(in1, in2, out)
      in1.setSignal(false)
      in2.setSignal(false)
      run
      
      assert(out.getSignal === false, "or 1")

      in1.setSignal(true)
      run
      
      assert(out.getSignal === true, "or 2")

      in2.setSignal(true)
      run
      
      assert(out.getSignal === true, "or 3")
    }

    test("orGate2 example") {
      val in1, in2, out = new Wire
      orGate2(in1, in2, out)
      in1.setSignal(false)
      in2.setSignal(false)
      run
      
      assert(out.getSignal === false, "or 1")

      in1.setSignal(true)
      run
      
      assert(out.getSignal === true, "or 2")

      in2.setSignal(true)
      run
      
      assert(out.getSignal === true, "or 3")
    }
    
    test("demux"){   
     val nrC=5                        // nr of control wires
     val nrOW=math.pow(2,nrC).toInt   // nr of output wires
    
     val in=new Wire()      
     val controlWires=listOfWires(nrC)    // control wires      
     val outWires=listOfWires(nrOW)       // output wires
    
     demux(in,controlWires,outWires)     // setting up demux circuit

     for(outWire <- 0 to (nrOW-1)){      //check for all output Wires    
           setControlWires(controlWires,outWire)   //set the control wires so that we get signal at given outWire   
           in.setSignal(true)      
           run
           val outShouldBe=getOutSignals(nrC,outWire)   //expected outcomes   
           assert(outWires.zip(outShouldBe).forall{case(wire,b)=>(wire.getSignal==b)&in.getSignal}) 
     }
  }

  def setControlWires(cws:List[Wire],cSig:Int)=    
     for(cw <- cws) cw.setSignal((cSig/(math.pow(2,cws.size-1-cws.indexOf(cw)).toInt)%2)==1)

  def getOutSignals(nrC:Int,cSig:Int):List[Boolean]=    
      (for(i<-1 to math.pow(2,nrC).toInt) yield(i==cSig+1)).toList.reverse
   
  def listOfWires(nr:Int):List[Wire]=(for(i<-1 to nr) yield new Wire).toList

}
