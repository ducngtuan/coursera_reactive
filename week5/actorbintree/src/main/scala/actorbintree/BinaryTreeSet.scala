/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case operation: Operation => root.forward(operation)
    case GC => {
      val newRoot = context.actorOf(BinaryTreeNode.props(0, true))
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot))
    }
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case msg: Operation => pendingQueue = pendingQueue.enqueue(msg)
    case CopyFinished => {
      // kill old root and replace with the new root
      root ! PoisonPill
      root = newRoot
      // forward all queued messages to the root
      for (msg <- pendingQueue) root ! msg
      pendingQueue = Queue.empty[Operation]

      context.become(normal)
    }
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case msg@Insert(requester, id, value) => {
      if (value == elem) {
        if (removed) {
          removed = false
        }
        requester ! OperationFinished(id)
      } else {
        val position = if (value < elem) Left else Right
        if (!subtrees.contains(position)) {
          subtrees += position -> context.actorOf(props(value, false))
          requester ! OperationFinished(id)
        } else {
          subtrees(position) ! msg
        }
      }
    }
    case msg@Contains(requester, id, value) => {
      if (value == elem) {
        requester ! ContainsResult(id, !removed)
      } else {
        val position = if (value < elem) Left else Right
        if (subtrees.contains(position)) {
          subtrees(position) ! msg
        } else {
          requester ! ContainsResult(id, false)
        }
      }
    }
    case msg@Remove(requester, id, value) => {
      if (value == elem) {
        removed = true
        requester ! OperationFinished(id)
      } else {
        val position = if (value < elem) Left else Right
        if (subtrees.contains(position)) {
          subtrees(position) ! msg
        } else {
          requester ! OperationFinished(id)
        }
      }
    }
    case CopyTo(_) if removed && subtrees.isEmpty => context.parent ! CopyFinished
    case msg @ CopyTo(node) => {
      if (!removed) {
        node ! Insert(self, -1, elem)
      }
      for (subtree <- subtrees.values) subtree ! msg
      context.become(copying(subtrees.values.toSet, removed))
    }
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case CopyFinished => {
      val newExpected = expected - sender
      if (newExpected.isEmpty && insertConfirmed) {
        context.parent ! CopyFinished
      } else {
        context.become(copying(newExpected, insertConfirmed))
      }
    }
    case OperationFinished(_) => {
      if (expected.isEmpty) {
        context.parent ! CopyFinished
      } else {
        context.become(copying(expected, true))
      }
    }
  }
}
