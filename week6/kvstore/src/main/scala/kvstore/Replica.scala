package kvstore

import akka.actor.{ OneForOneStrategy, Props, ActorRef, Actor }
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.{Escalate, Restart}
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import akka.actor.Terminated
import scala.concurrent.duration._
import akka.actor.PoisonPill
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.util.Timeout

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  case class PersistTimeout(id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  var persistence = context.actorOf(persistenceProps)
  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]

  override def preStart() {
    arbiter ! Join
  }

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1.minute) {
      case _: PersistenceException => Restart
      case _: Exception => Escalate
    }

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica(0))
  }

  /** Receive for Get messages for both primary and secondary replica */
  val receiveGet: Receive = {
    case Get(key, id) => sender ! GetResult(key, kv.get(key), id)
  }

  /** Receive for Insert and Remove messages for primary replica */
  val receiveInsertRemove: Receive = {
    case Insert(key, value, id) =>
      kv += (key -> value)
      self ! (sender, Persist(key, Some(value), id))
    case Remove(key, id) =>
      kv -= key
      self ! (sender, Persist(key, None, id))
  }

  // Map of pending operation from operation `id` to tuple of
  // requester, persisted status, and set of unconfirmed replicators.
  var pending = Map.empty[Long, (ActorRef, Boolean, Set[ActorRef])]

  val leader: Receive = receiveGet orElse receiveInsertRemove orElse {
    case Replicas(replicas) =>
      val (remain, removed) =
        secondaries.partition { case (r, _) => replicas.contains(r) }

      // creating new replicators for newly added secondaries
      val newReplicas = (for {
        replica <- replicas if !remain.contains(replica) && replica != self
      } yield (replica, context.actorOf(Replicator.props(replica)))).toMap
      // replicate all existing contents
      for ((k, v) <- kv; replicator <- newReplicas.values) {
        replicator ! Replicate(k, Some(v), -1)
      }

      // update list of secondaries and replicators
      secondaries = remain ++ newReplicas
      replicators = secondaries.values.toSet

      // stop replication to removed secondaries by sending a "false" ack
      // from the replicator to `self` as if the replication succeeded
      for (r <- removed.values; id <- pending.keys) {
        self.tell(Replicated("", id), r)
      }
      // now terminate those
      removed.values.foreach(context.stop)

    case (requester: ActorRef, Persist(key, valueOption, id)) =>
      pending += (id -> (requester, false, replicators))

      // ask persistence to persist the change
      context.actorOf(RetryWorker.props(
        persistence, Persist(key, valueOption, id), 100.milliseconds))
      // broadcast the change to replicators
      replicators foreach (_ ! Replicate(key, valueOption, id))
      // fail the operation after 1 second if there is no confirmation
      context.system.scheduler.scheduleOnce(1.second, self, PersistTimeout(id))

    case PersistTimeout(id) =>
      // fail the operation, but do not remove `id` from `pending` since we
      // still want to ask for persistence and replications in background
      pending.get(id) foreach (_._1 ! OperationFailed(id))

    case Persisted(key, id) => pending.get(id) match {
      case Some((requester, _, waitingList)) =>
        if (waitingList.isEmpty) {
          requester ! OperationAck(id)
          pending -= id
        } else {
          pending += (id -> (requester, true, waitingList))
        }
      case None =>
    }

    case Replicated(key, id) => pending.get(id) match {
      case Some((requester, persisted, waitingList)) =>
        val remain = waitingList - sender
        if (remain.isEmpty && persisted) {
          requester ! OperationAck(id)
          pending -= id
        } else {
          pending += (id -> (requester, persisted, remain))
        }
      case None =>
    }
  }

  /** Receive for secondary replica that deals with simple Snapshot messages */
  def replicaBasic(expectedSeq: Long): Receive = {
    case Snapshot(key, _, seq) if seq > expectedSeq => // do nothing
    case Snapshot(key, _, seq) if seq < expectedSeq =>
      sender ! SnapshotAck(key, seq)
  }

  /** Receive for secondary replica that waiting for persisted messages */
  def replicaPending(expectedSeq: Long, requester: ActorRef): Receive =
    receiveGet orElse replicaBasic(expectedSeq) orElse {
      case Persisted(key, seq) =>
        requester ! SnapshotAck(key, seq)
        context.become(replica(Math.max(seq + 1, expectedSeq)))
    }

  def replica(expectedSeq: Long): Receive =
    receiveGet orElse replicaBasic(expectedSeq) orElse {
      case Snapshot(key, valueOption, seq) =>
        valueOption match {
          case Some(value) => kv += (key -> value)
          case None => kv -= key
        }
        context.actorOf(RetryWorker.props(
          persistence, Persist(key, valueOption, seq), 100.milliseconds))
        context.become(replicaPending(expectedSeq, sender))
  }

}
