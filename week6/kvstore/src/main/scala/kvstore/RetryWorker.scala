package kvstore

import akka.actor.{Props, ReceiveTimeout, ActorRef, Actor}
import scala.concurrent.duration.FiniteDuration

object RetryWorker {
  case object MaxAttemptsReached

  /** Create a retry worker that ask infinitely until get an answer. */
  def props(to: ActorRef, msg: Any, interval: FiniteDuration): Props =
    Props(new RetryWorker(to, msg, interval, -1, None))

  /** Create a retry worker that ask for a finite times before return.
    *
    * @param to        receiver of the message
    * @param msg       message to send
    * @param interval  duration between each attempt
    * @param attempts  number of attempts
    * @param onFailure value to return to parent actor when done
    */
  def props(to: ActorRef, msg: Any, interval: FiniteDuration,
            attempts: Int, onFailure: Any = MaxAttemptsReached): Props =
    Props(new RetryWorker(to, msg, interval, attempts, onFailure))
}

class RetryWorker(to: ActorRef, msg: Any, interval: FiniteDuration,
                  private var attempts: Int = 1,
                  onFailure: Any = RetryWorker.MaxAttemptsReached)
  extends Actor {

  override def preStart() {
    to ! msg
    context.setReceiveTimeout(interval)
  }

  val receive: Receive = {
    case ReceiveTimeout if attempts < 0 => to ! msg
    case ReceiveTimeout if attempts > 0 =>
      to ! msg
      attempts -= 1
    case ReceiveTimeout =>
      context.parent ! onFailure
      context.stop(self)
    case msg =>
      context.parent.forward(msg)
      context.stop(self)
  }
}
